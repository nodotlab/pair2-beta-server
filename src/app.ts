import ApiGateway from "./server/apiGateway";


class Main {
  constructor() {
    this.apiGateway = new ApiGateway(8081);
  }

  private apiGateway: ApiGateway;

  public update(): void {
    console.log("Main update 🦄 Resful serve updating... try ");
    this.apiGateway.update();
  }
}


new Main().update();