class StreamData<S, F, W, C> {
  public sneakers: S[];
  public filters: F[];
  public whitelist: W[];
  public findlist: W[];
  public cheatlist: C[];

  constructor() {
    this.sneakers = [];
    this.filters = [];
    this.whitelist = [];
    this.findlist = [];
    this.cheatlist = [];
  }

  public recurrentStream(newData: S[]): void {
    this.sneakers = newData;
  }
}

export default StreamData;