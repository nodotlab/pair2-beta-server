export interface Sneaker {
  index: number;
  brand: string;
  name: string;
  nameKr: string;
  releaseDate: number;
  releasePrice: number;
  dollar: boolean;
  serial: string;
}

export interface Filter {
  index: number;
  name: string;
  other: boolean;
}

export interface WhiteList {
  phone: string;
  code: string;
  verify: boolean;
  timestamp: number;
}

export interface Cheat {
  phone: string;
  cheat: boolean;
}