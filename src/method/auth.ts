import jwt from "jwt-simple";
import axios from "axios";
import cryptoJs from "crypto-js";
const sha512 = cryptoJs.SHA512;
const base64 = cryptoJs.enc.Base64;
import secret from "../config/token";
import { naverHeader, serviceId } from "../config/naver";


class Auth {
  public verifyHash(password: string, salt: string, hash: string, ) {
    if (sha512(password + salt).toString(base64) === hash) {
      return true;
    } else {
      return false;
    }
  }

  public newToken(index: number): string {
    const token = jwt.encode({
      index: index,
      timestamp: Date.now() + (60000 * 60 * 24 * 7)
    }, secret);

    return token;
  } // when try new sign token longterm

  public decodeToken(token: string): boolean {
    try {
      const decode = jwt.decode(token, secret);
      if (decode.timestamp > Date.now()) {
        return decode;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  public naverSms = async (phone: string, randomNumber: string) => {
    const data = {
      "type": "SMS",
      "contentType": "COMM",
      "countryCode": "82",
      "from": "07042560099", // from 은 임의로 변경 불가
      "to": [
        phone
      ],
      "subject": "PAIR2 인증번호",
      "content": `[${randomNumber}] 한정판 스니커즈 발매정보 공유 서비스 PAIR2 인증번호입니다.`
    };

    const result = await axios.post(`https://api-sens.ncloud.com/v1/sms/services/${serviceId}/messages`, data, naverHeader);

    return result.data;
  };

  public smsSend = async (phone: string, title: string, content: string) => {
    const data = {
      "type": "LMS",
      "contentType": "COMM",
      "countryCode": "82",
      "from": "07042560099",
      "to": [
        phone
      ],
      "subject": title,
      "content": content
    };

    const result = await axios.post(`https://api-sens.ncloud.com/v1/sms/services/${serviceId}/messages`, data, naverHeader);

    return result.data;
  };
}


export default new Auth;
