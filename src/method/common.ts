export const searchWord = (word: string): string => {
  let result = word.toUpperCase();
  result = result.replace(/(\s*)/g, "");

  return result;
};

export const replaceWord = (word: string): string => {
  return word.replace(/(\s*)/g, "");
};

export const randomNumber = (): string => {
  const n: number = Math.floor(Math.random() * (1000000 - 100000)) + 100000;
  return n.toString();
};

export const randomInt = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min)) + min;
  // 최댓값은 제외, 최솟값은 포함;
};

export const shuffle = (arr: any): any => {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
};

export const makeCode = (): string => {
  const wordList: string[] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
  "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
  "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];

  let code: string = "";

  for (let i = 0; i < 9; ++i) {
    code = code + wordList[randomInt(0, wordList.length)];
  }

  return code;
};


export const hex2bin = (bin: string): string => {
  let i = 0;
  const l = bin.length;
  let chr = undefined;
  let hex = "";

  for (i; i < l; ++i) {
    chr = bin.charCodeAt(i).toString(16);
    hex += chr.length < 2 ? "0" + chr : chr;
  }

  return hex;
}