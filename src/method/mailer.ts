import nodemailer from "nodemailer";


class Mailer {
  private transporter: any;

  constructor() {
    this.transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        user: "pair2cs@gmail.com",
        pass: "shekt123!",
      }
    });
  }

  public sendEmail(title: string, body: string): void {
    const mailOptions = {
      from: "pair2cs@gmail.com",
      to: "support@pair2.xyz", //
      subject: title,
      html: body,
    };

    this.transporter.sendMail(mailOptions, (err: any, data: any) => {
      if (err) {
        console.log("Email send error", err);
      } else {
        console.log("Email sent");
      }
    });
  }
}


export default new Mailer();