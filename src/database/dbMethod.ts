import * as AWS from "aws-sdk";
import { dynamoConfig } from "../config/aws";
import * as _ from "lodash";

const db = new AWS.DynamoDB.DocumentClient(dynamoConfig);

class DbMethod {
  constructor() { }

  public async getUserNotificationByUUID(body: any){
    const params = {
      TableName : "users",
      FilterExpression: "#UUID = :UUID",
      ExpressionAttributeNames: {
        "#UUID": "UUID"
      },
      ExpressionAttributeValues: {
        ":UUID": body.UUID
      }
    };

    return db.scan(params).promise();
  }

  public async searchUserByWord(body:any){
    const params = {
      TableName : "users",
      FilterExpression: "contains(#name, :searchWord) or contains(size, :searchWord) or" +
          " contains(birthDate, :searchWord) or contains(sex, :searchWord) or" +
          " contains(deviceModel, :searchWord) or contains(#UUID, :searchWord) or" +
          " contains(email, :searchWord) or contains(nickname, :searchWord) or" +
          " contains(birthMonth, :searchWord) or contains(deviceOS, :searchWord) or" +
          " contains(phone, :searchWord) or contains(age, :searchWord)",
      ExpressionAttributeNames: {
        "#name": "name",
        "#UUID": "UUID"
      },
      ExpressionAttributeValues: {
        ":searchWord": body.searchWord
      }
    };

    return db.scan(params).promise();
  }
  public async searchUserByName(body:any){

    const params = {
      TableName : "users",
      FilterExpression: "contains(#name, :searchName)",
      ExpressionAttributeNames: {
        "#name": "name",
      },
      ExpressionAttributeValues: {
        ":searchName": body.searchName
      }
    };

    return db.scan(params).promise();

  }

  public async updateUserLogin(refreshBody:any){
    const params = {
      TableName: "users",
      Key: {
        index: refreshBody.index
      },
      UpdateExpression:
          `set deviceToken = :deviceToken, #UUID = :UUID, notification = :notification`,
      ExpressionAttributeNames: {
        "#UUID": "UUID",
      },
      ExpressionAttributeValues: {
        ":deviceToken": refreshBody.deviceToken,
        ":UUID": refreshBody.UUID,
        ":notification": refreshBody.notification
      },
      ReturnValues: "UPDATED_NEW"
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async getUserList() {
    const params = {
      TableName: "users"
    };
    const result = await db.scan(params).promise();

    return result.Items;
  }

  public async saveUserDeviceToken(body: any){
    const params = {
      TableName: "users",
      Key: {
        index: body.index
      },
      UpdateExpression:
          `set deviceToken = :deviceToken, #UUID = :UUID, deviceOS = :deviceOS, deviceModel = :deviceModel`,
      ExpressionAttributeNames: {
        "#UUID": "UUID",
      },
      ExpressionAttributeValues: {
        ":deviceToken": body.deviceToken,
        ":deviceOS": body.deviceOS,
        ":UUID" : body.UUID,
        ":deviceModel": body.deviceModel
      },
      ReturnValues: "UPDATED_NEW"
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async setUserNotificationTrue(body: any){
    const params = {
      TableName: "users",
      Key: {
        index: body.index
      },
      UpdateExpression:
          `set notification = :true`,
      ExpressionAttributeValues: {
        ":true": true
      },
      ReturnValues: "UPDATED_NEW"
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async setUserNotificationFalse(body: any){
    const params = {
      TableName: "users",
      Key: {
        index: body.index
      },
      UpdateExpression:
          `set notification = :false`,
      ExpressionAttributeValues: {
        ":false": false
      },
      ReturnValues: "UPDATED_NEW"
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async getUserInformation(body: any){
    const params = {
      TableName : "users",
      Key: {
        index: body.index
      }
    };

    const result = await db.get(params).promise();
    return result;
  }

  public async getByIndex(table: string, index: number) {
    const params = {
      TableName: table,
      Key: {
        index: index,
      },
    };

    // try {
    const result = await db.get(params).promise();
    return result;
    // } catch (error) {
    //   console.log(error);
    //   return error;
    // }
  }

  public async getUser(index: number) {
    const params = {
      TableName: "users",
      ProjectionExpression: "nickname, phone, email, userVerify",
      Key: {
        index: index,
      },
    };

    const result = await db.get(params).promise();
    return result;
  }

  public async getPhone(index: number) {
    const params = {
      TableName: "users",
      ProjectionExpression: "phone",
      Key: {
        index: index,
      },
    };

    const result: any = await db.get(params).promise();
    return result.Item.phone;
  }

  public async getEmail(index: number) {
    const params = {
      TableName: "users",
      ProjectionExpression: "email",
      Key: {
        index: index,
      },
    };

    const result: any = await db.get(params).promise();
    return result.Item.email;
  }
  

  public async getShippings(index: number) {
    const params = {
      TableName: "users",
      ProjectionExpression: "shippings",
      Key: {
        index: index,
      },
    };

    const result: any = await db.get(params).promise();
    return result.Item;
  }

  public async getBank(index: number) {
    const params = {
      TableName: "users",
      ProjectionExpression: "bank",
      Key: {
        index: index,
      },
    };

    const result: any = await db.get(params).promise();
    return result.Item;
  }

  public async getPayment(index: number) {
    const params = {
      TableName: "users",
      ProjectionExpression: "payment",
      Key: {
        index: index,
      },
    };

    const result: any = await db.get(params).promise();
    return result.Item.payment;
  }

  public async getAsk(askIndex: number) {
    const params = {
      TableName: "ask",
      KeyConditionExpression: "#index = :index",
      ExpressionAttributeNames: {
        "#index": "index",
      },
      ExpressionAttributeValues: {
        ":index": askIndex,
      },
    };

    const result: any = await db.query(params).promise();
    return result.Items[0];
  }

  public async queryExistLikes(userIndex: number, data: any) {
    const params = {
      TableName: "likes",
      IndexName: "user-userSort-index",
      ProjectionExpression: "size, #name",
      KeyConditionExpression: "#user = :user",
      ExpressionAttributeNames: {
        "#user": "user",
        "#name": "name",
      },
      FilterExpression: "#name = :name and size = :size",
      ExpressionAttributeValues: {
        ":user": userIndex,
        ":size": data.size,
        ":name": data.name,
      },
      // Limit: 1,
    };

    const result: any = await db.query(params).promise();
    if (result.Count > 0) {
      return true;
    } else {
      return false;
    }
  } // for the exists filtering

  public async queryExistLikesTrade(user: number, tradeIndex: number) {
    const params = {
      TableName: "likestrade",
      IndexName: "user-sort-index",
      KeyConditionExpression: "#user = :user",
      ExpressionAttributeNames: {
        "#user": "user",
      },
      FilterExpression: "tradeIndex = :tradeIndex",
      ExpressionAttributeValues: {
        ":user": user,
        ":tradeIndex": tradeIndex,
      },
    };

    const result: any = await db.query(params).promise();
    if (result.Count > 0) {
      return true;
    } else {
      return false;
    }
  } // for the exists filtering

  public async queryExistLikeName(userIndex: number, name: string) {
    const params = {
      TableName: "likes",
      IndexName: "user-userSort-index",
      ProjectionExpression: "#name",
      KeyConditionExpression: "#user = :user",
      ExpressionAttributeNames: {
        "#user": "user",
        "#name": "name",
      },
      FilterExpression: "#name = :name",
      ExpressionAttributeValues: {
        ":user": userIndex,
        ":name": name,
      },
      // Limit: 1,
    };

    const result: any = await db.query(params).promise();

    if (result.Count > 0) {
      return true;
    } else {
      return false;
    }
  }

  public async queryMyLikes(userIndex: number, limit: number) {
    const params = {
      TableName: "likes",
      IndexName: "user-userSort-index",
      ProjectionExpression: "#index, thumbUrl, brand, size, #name",
      KeyConditionExpression: "#user = :user",
      ExpressionAttributeNames: {
        "#index": "index",
        "#user": "user",
        "#name": "name",
      },
      ExpressionAttributeValues: {
        ":user": userIndex,
      },
      Limit: limit,
      ScanIndexForward: false,
    };

    const result: any = await db.query(params).promise();
    return result.Items;
  }

  public async queryMyLikesTrade(userIndex: number, limit: number) {
    const params = {
      TableName: "likestrade",
      IndexName: "user-sort-index",
      KeyConditionExpression: "#user = :user",
      ExpressionAttributeNames: {
        "#user": "user",
      },
      ExpressionAttributeValues: {
        ":user": userIndex,
      },
      Limit: limit,
    };

    const result: any = await db.query(params).promise();
    return result.Items;
  }

  public async queryTradeList(sneakerIndex: number, limit: number) {
    const params = {
      TableName: "trade",
      IndexName: "sneakerIndex-sort-index",
      ProjectionExpression:
        "#index, #from, #step, damage1, damage2, damage3, damage4, damageDesc, status1, status2, status3, statusDesc, shippingType, price, imageUrls, size, seller",
      KeyConditionExpression: "sneakerIndex = :searchIndex",
      FilterExpression: "complete = :complete AND #step = :step",
      ExpressionAttributeNames: {
        "#index": "index",
        "#from": "from",
        "#step": "step",
      },
      ExpressionAttributeValues: {
        ":searchIndex": sneakerIndex,
        ":complete": false,
        ":step": 0,
      },
      Limit: limit,
      ScanIndexForward: false,
    };

    const result: any = await db.query(params).promise();
    return result.Items;
  }

  public async queryTradePrice(sneakerIndex: number) {
    const params = {
      TableName: "trade",
      IndexName: "sneakerIndex-sort-index",
      KeyConditionExpression: "sneakerIndex = :searchIndex",
      ExpressionAttributeValues: {
        ":searchIndex": sneakerIndex,
      },
      ScanIndexForward: false,
    };

    const result: any = await db.query(params).promise();
    return result.Items;
  }

  public async queryTradeIndex(tradeIndex: number) {
    const params = {
      TableName: "trade",
      ProjectionExpression: "imageUrls",
      KeyConditionExpression: "#index = :index",
      ExpressionAttributeNames: {
        "#index": "index",
      },
      ExpressionAttributeValues: {
        ":index": tradeIndex,
      },
    };

    const result: any = await db.query(params).promise();
    const imageUrl = result.Items[0].imageUrls[0];
    return imageUrl;
  } // use for buyer images

  public async queryTradeIndexCompleteTimestamp(tradeIndex: number) {
    const params = {
      TableName: "trade",
      ProjectionExpression: "completeTimestamp",
      KeyConditionExpression: "#index = :index",
      ExpressionAttributeNames: {
        "#index": "index",
      },
      ExpressionAttributeValues: {
        ":index": tradeIndex,
      },
    };

    const result: any = await db.query(params).promise();
    const timestamp = result.Items[0].completeTimestamp;
    return timestamp;
  } // use for buyer images

  public async querySneakerIndex(sneakerIndex: number) {
    const params = {
      TableName: "sneakers",
      ProjectionExpression: "#name, brand",
      KeyConditionExpression: "#index = :index",
      ExpressionAttributeNames: {
        "#index": "index",
        "#name": "name",
      },
      ExpressionAttributeValues: {
        ":index": sneakerIndex,
      },
    };

    const result: any = await db.query(params).promise();
    return result.Items[0];
  } // use for buyer images

  public async queryPrice(sneakerIndex: number) {
    const params = {
      TableName: "trade",
      // ProjectionExpression: "sneakerIndex",
      IndexName: "sneakerIndex-index",
      KeyConditionExpression: "sneakerIndex = :sneakerIndex",
      ExpressionAttributeValues: {
        ":sneakerIndex": sneakerIndex,
      },
    };

    const result: any = await db.query(params).promise();
    // console.log(result);
    return result.Items;
  } // searching all price with sneaker index

  public async queryStore(releaseIndex: number) {

    const currentTimestamp: number = new Date().getTime();
    console.log(currentTimestamp);

    //현재 응모가 진행중이 스토어 목록
    const params: any = {
      TableName: "store",
      IndexName: "release-releaseDateEnd-index",
      KeyConditionExpression: "#release = :release AND #end >= :time",
      FilterExpression: "#start <= :time",
      ExpressionAttributeNames: {
        "#release": "release",
        "#start": "releaseDate",
        "#end": "releaseDateEnd",
      },
      ExpressionAttributeValues: {
        ":release": releaseIndex,
        ":time": currentTimestamp
      },
      ScanIndexForward: true
    };


    let result: any = await db.query(params).promise();
    let stores = result.Items;

    //응모시작 전 스토어 목록
    params.IndexName = "release-releaseDate-index";
    params.KeyConditionExpression = "#release = :release AND #start > :time";
    params.FilterExpression = null;
    params.ExpressionAttributeNames = { "#release": "release", "#start": "releaseDate" };
    params.ScanIndexForward = true;
    result = await db.query(params).promise();

    for await (let store of result.Items) {
      stores.push(store);
    }

    //응모 종료 스토어 목록
    params.IndexName = "release-releaseDateEnd-index";
    params.KeyConditionExpression = "#release = :release AND #end < :time";
    params.ExpressionAttributeNames = { "#release": "release", "#end": "releaseDateEnd" };
    params.ScanIndexForward = false;
    result = await db.query(params).promise();

    for await (let store of result.Items) {
      stores.push(store);
    }

    return stores;
  }

  //오늘의 발매 목록
  public async queryToday() {
    //오늘날짜 00:00:00 ~ 23:59:59
    let startDate = new Date();
    startDate.setHours(0, 0, 0, 0);

    let endDate = new Date();
    endDate.setHours(23, 59, 59, 59)

    const startTime: number = startDate.getTime();
    const endTime: number = endDate.getTime();

    console.log('start time : ' + startTime + ' end time : ' + endTime);

    //오늘자 발매처 목록 쿼리
    let params: any = {
      TableName: "store",
      ProjectionExpression: "#snkrIdx",
      FilterExpression: "#start between :start and :end or #end between :start and :end",
      ExpressionAttributeNames: {
        "#snkrIdx": "release",
        "#start": "releaseDate",
        "#end": "releaseDateEnd"
      },
      ExpressionAttributeValues: {
        ":start": startTime,
        ":end": endTime
      }
    };

    const snkrs: any = await db.scan(params).promise();
    const todaySnkrsIdx = _.map(_.uniqBy(snkrs.Items, 'release'), 'release');

    // console.log("today distinct snkrs: ", todaySnkrsIdx);

    params.TableName = "release";
    params.ProjectionExpression = null;
    params.FilterExpression = "#snkrsIdx = :idx"
    params.ExpressionAttributeNames = { "#snkrsIdx": "index" };

    const result: any = [];
    for (const idx of todaySnkrsIdx) {
      params.ExpressionAttributeValues = {
        ":idx": idx
      };

      const resultTmp = await db.scan(params).promise();

      //현재 상태 불러오기, 어디 응모중, 응모 종료, 응모 임박
      if (resultTmp.Items?.[0]) {
        result.push(resultTmp.Items?.[0]);
      }
    }

    // console.log(result)
    return result;
  }


  /**
   * 스토어 기준으로 오늘의 발매 목록 뽑기
   */
  public async queryTodaybyStore() {
    //오늘날짜 00:00:00 ~ 23:59:59
    let startDate = new Date();
    startDate.setHours(0, 0, 0, 0);

    let endDate = new Date();
    endDate.setHours(23, 59, 59, 59)

    const startTime: number = startDate.getTime();
    const endTime: number = endDate.getTime();

    // console.log('start time : ' + startTime + ' end time : ' + endTime);

    

    //오늘자 발매처 목록 쿼리
    let params: any = {
      TableName: "store",
      FilterExpression: "(#start between :start and :end) or (#end between :start and :end) or (#start < :end and #end > :end)",
      ExpressionAttributeNames: {
        "#start": "releaseDate",
        "#end": "releaseDateEnd"
      },
      ExpressionAttributeValues: {
        ":start": startTime,
        ":end": endTime
      }
    };

    // const stores:any = [];
    // await db.scan(params, function isLastScan (err, data) {
    //   if (err) {
    //     console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
    //   } else {
        
    //     console.log("data :", data);
    //     if (typeof data.LastEvaluatedKey != "undefined") {
    //       console.log("Scanning for more...", data.LastEvaluatedKey);
          
    //       params.ExclusiveStartKey = data.LastEvaluatedKey;
    //       db.scan(params, isLastScan).promise();
    //     } 
    //   }
    // }).promise();

    const stores:any = [];
    
    let tmp: any = await db.scan(params).promise();
    stores.push(... tmp.Items);

    
    
    if(tmp.LastEvaluatedKey != undefined) {
      params.ExclusiveStartKey = tmp.LastEvaluatedKey;
      tmp = await db.scan(params).promise();
      stores.push(... tmp.Items);
    }
    
    let queryStart = new Date().getTime();
    
    // 스니커즈 정보 쿼리
    let snkrsParams:any = {
      TableName: "release",
      FilterExpression: "#snkrsIdx = :idx",
      ExpressionAttributeNames: { "#snkrsIdx": "index" }
    }
    
    
    
    for (const store of stores) {
      snkrsParams.ExpressionAttributeValues = {
        ":idx": store.release
      };
      const resultTmp = await db.scan(snkrsParams).promise();
      store.snkrs = resultTmp.Items?.[0];
    }
    
    let queryEnd = new Date().getTime();
    
    console.log('API 쿼리 시간: '+(queryEnd-queryStart)/1000+'초');
    // console.log("today release : ", stores);
    // console.log("tmp : ", tmp);

    
    return stores;
  }

  public async scanFull(table: string) {
    const params = {
      TableName: table,
    };

    const result = await db.scan(params).promise();
    return result;
  }

  public async scanItem(table: string, field: string, value: string) {
    const params = {
      TableName: table,
      ProjectionExpression: field,
      FilterExpression: `${field} = :${field}`, // "phone = :phone",
      ExpressionAttributeValues: {
        [`:${field}`]: value,
      },
    };

    const result = await db.scan(params).promise();
    return result;
  }

  public async scanUser(phone: string) {
    const params = {
      TableName: "users",
      ProjectionExpression: "#index, #hash, salt",
      ExpressionAttributeNames: {
        "#index": "index",
        "#hash": "hash",
      },
      FilterExpression: `phone = :phone`, // "phone = :phone",
      ExpressionAttributeValues: {
        ":phone": phone,
      },
    };

    const result: any = await db.scan(params).promise();
    return result.Items[0];
  }

  public async scanUserByEmail(email: string) {
    const params = {
      TableName: "users",
      ProjectionExpression: "#index, #hash, salt",
      ExpressionAttributeNames: {
        "#index": "index",
        "#hash": "hash",
      },
      FilterExpression: `email = :email`, // "phone = :phone",
      ExpressionAttributeValues: {
        ":email": email,
      },
    };

    const result: any = await db.scan(params).promise();
    return result.Items[0];
  }

  public async scanNameWithPhone(phone: string) {
    const params = {
      TableName: "users",
      ProjectionExpression: "phone, #name",
      ExpressionAttributeNames: {
        "#name": "name",
      },
      FilterExpression: "phone = :phone",
      ExpressionAttributeValues: {
        ":phone": phone,
      },
    };

    const result: any = await db.scan(params).promise();
    return result.Items[0];
  }

  public async scanUserIndex(phone: string) {
    const params = {
      TableName: "users",
      ProjectionExpression: "#index",
      ExpressionAttributeNames: {
        "#index": "index",
      },
      FilterExpression: "phone = :phone",
      ExpressionAttributeValues: {
        ":phone": phone,
      },
    };

    const result: any = await db.scan(params).promise();
    return result.Items[0].index;
  }

  public async scanBuyer(table: string, buyerIndex: string) {
    const params = {
      TableName: table,
      FilterExpression: `buyer = :buyer`,
      ExpressionAttributeValues: {
        ":buyer": buyerIndex,
      },
    };

    const result = await db.scan(params).promise();
    return result.Items;
  }

  public async scanSeller(sellerIndex: number) {
    const params = {
      TableName: "trade",
      FilterExpression: `seller = :seller`,
      ExpressionAttributeValues: {
        ":seller": sellerIndex,
      },
    };

    try {
      const result = await db.scan(params).promise();
      // console.log(result.Items);
      return result.Items;
    } catch (error) {
      console.log(error);
      return [];
    }
  }

  public async scanSellerAsk(sellerIndex: number) {
    const params = {
      TableName: "ask",
      // FilterExpression: `seller = :seller AND #status = :status`,
      FilterExpression: `seller = :seller`,
      // ExpressionAttributeNames: {
      //   "#status": "status"
      // },
      ExpressionAttributeValues: {
        ":seller": sellerIndex,
        // ":status": "거래요청중"
      },
    };

    try {
      const result = await db.scan(params).promise();
      return result.Items;
    } catch (error) {
      console.log(error);
      return [];
    }
  }

  public async scanAskExist(tradeIndex: number) {
    const params = {
      TableName: "ask",
      FilterExpression: `tradeIndex = :tradeIndex AND #status = :status`,
      ExpressionAttributeNames: {
        "#status": "status",
      },
      ExpressionAttributeValues: {
        ":tradeIndex": tradeIndex,
        ":status": "거래요청중",
      },
    };

    const result = await db.scan(params).promise();
    return result.Count;
  }

  public async scanAskMyExist(tradeIndex: number, buyer: number) {
    const params = {
      TableName: "ask",
      FilterExpression: `tradeIndex = :tradeIndex AND #buyer = :buyer`,
      ExpressionAttributeNames: {
        "#buyer": "buyer",
      },
      ExpressionAttributeValues: {
        ":tradeIndex": tradeIndex,
        ":buyer": buyer,
      },
    };

    const result: any = await db.scan(params).promise();
    let exist: boolean = false;

    for (let i in result.Items) {
      if (result.Items[i].status === "거래요청중") exist = true;
    }

    return exist;
  }

  public async put(table: string, data: object) {
    const params = {
      TableName: table,
      Item: { ...data },
    };

    try {
      const result = await db.put(params).promise();
      return result;
    } catch (error) {
      console.log(error);
      return "success";
    }
  }

  public async delete(table: string, index: number) {
    const params = {
      TableName: table,
      Key: {
        index: index,
      },
    };

    const result = await db.delete(params).promise();
    return result;
  }

  public async editSneaker(body: any) {
    const params = {
      TableName: "sneakers",
      Key: {
        index: body.index,
      },
      UpdateExpression:
        "set #name = :n, nameKr = :nk, brand = :b, dollar = :d, releaseDate = :r, releasePrice = :p, serial = :s, thumbUrl = :t, imageUrl1 = :i1, imageUrl2 = :i2, imageUrl3 = :i3, imageUrl4 = :i4, imageUrl5 = :i5",
      ExpressionAttributeNames: {
        "#name": "name",
      },
      ExpressionAttributeValues: {
        ":n": body.name,
        ":nk": body.nameKr,
        ":b": body.brand,
        ":d": body.dollar,
        ":r": parseInt(body.releaseDate),
        ":p": parseInt(body.releasePrice),
        ":s": body.serial,
        ":t": body.thumbUrl ? body.thumbUrl : false,
        ":i1": body.imageUrl1 ? body.imageUrl1 : false,
        ":i2": body.imageUrl2 ? body.imageUrl2 : false,
        ":i3": body.imageUrl3 ? body.imageUrl3 : false,
        ":i4": body.imageUrl4 ? body.imageUrl4 : false,
        ":i5": body.imageUrl5 ? body.imageUrl5 : false,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    // console.log(result);
    return result;
  }

  public async editUserNew(body: any) {

    console.log("data:",body);

    const params = {
      TableName: "users",
      Key: {
        index: body.index
      },
      UpdateExpression:
        "set #hash = :hash, salt = :salt, nickname = :nickname, email = :email, agree1 = :agree1, agree2 = :agree2",
      ExpressionAttributeNames: {
        "#hash": "hash"
      },
      ExpressionAttributeValues: {
        ":hash": body.hash,
        ":salt": body.salt,
        ":nickname": body.nickname,
        ":email": body.email,
        ":agree1": body.agree1,
        ":agree2": body.agree2
      },
      ReturnValues: "UPDATED_NEW",
    };

    console.log("here: ", body.index);
    
    const result = await db.update(params, function(err){
      if(err){
        console.error(err);
      }
    }).promise();
    return result;
  }

  public async editUser(body: any) {
    const params = {
      TableName: "users",
      Key: {
        index: body.index,
      },
      UpdateExpression:
        "set #name = :name, #hash = :hash, salt = :salt, phone = :phone, nickname = :nickname, email = :email, age = :age, birthMonth = :birthMonth, birthDate = :birthDate, sex = :sex, size = :size, smsAgree = :smsAgree, emailAgree = :emailAgree, agree1 = :agree1, agree2 = :agree2, agree3 = :agree3",
      ExpressionAttributeNames: {
        "#name": "name",
        "#hash": "hash",
      },
      ExpressionAttributeValues: {
        ":name": body.name,
        ":hash": body.hash,
        ":salt": body.salt,
        ":phone": body.phone,
        ":nickname": body.nickname,
        ":email": body.email,
        ":age": body.age,
        ":birthMonth": body.birthMonth,
        ":birthDate": body.birthDate,
        ":sex": body.sex,
        ":size": body.size,
        ":smsAgree": body.smsAgree,
        ":emailAgree": body.emailAgree,
        ":agree1": body.agree1,
        ":agree2": body.agree2,
        ":agree3": body.agree3,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async editUserVerify(index: number) {
    const params = {
      TableName: "users",
      Key: {
        index: index,
      },
      UpdateExpression: "set userVerify = :userVerify",
      ExpressionAttributeValues: {
        ":userVerify": true,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async editPassword(index: number, hash: string, salt: string) {
    const params = {
      TableName: "users",
      Key: {
        index: index,
      },
      UpdateExpression: "set #hash = :h, salt = :s",
      ExpressionAttributeNames: {
        "#hash": "hash",
      },
      ExpressionAttributeValues: {
        ":h": hash,
        ":s": salt,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async editShippings(index: any, shippings: any) {
    const params = {
      TableName: "users",
      Key: {
        index: index,
      },
      UpdateExpression: "set shippings = :s",
      ExpressionAttributeValues: {
        ":s": shippings,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async editBank(index: any, bank: any) {
    const params = {
      TableName: "users",
      Key: {
        index: index,
      },
      UpdateExpression: "set bank = :b",
      ExpressionAttributeValues: {
        ":b": bank,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();

    return result;
  }

  public async editPayment(index: any, payment: any) {
    const params = {
      TableName: "users",
      Key: {
        index: index,
      },
      UpdateExpression: "set payment = :p",
      ExpressionAttributeValues: {
        ":p": payment,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async editAsk(askIndex: number, status: string) {
    const params = {
      TableName: "ask",
      Key: {
        index: askIndex,
      },
      UpdateExpression:
        "set #status = :status, #acceptTimestamp = :acceptTimestamp",
      ExpressionAttributeNames: {
        "#status": "status",
        "#acceptTimestamp": "acceptTimestamp",
      },
      ExpressionAttributeValues: {
        ":status": status,
        ":acceptTimestamp": Date.now(),
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async editTrade(body: any) {
    const params = {
      TableName: "trade",
      Key: {
        index: body.index,
      },
      UpdateExpression:
        "set #from = :from, damage1 = :damage1, damage2 = :damage2, damage3 = :damage3, damage4 = :damage4, damageDesc = :damageDesc, status1 = :status1, status2 = :status2, status3 = :status3, statusDesc = :statusDesc, imageUrls = :imageUrls, shippingType = :shippingType, price = :price, location1 = :location1, location2 = :location2",
      ExpressionAttributeNames: {
        "#from": "from",
      },
      ExpressionAttributeValues: {
        ":from": body.from,
        ":damage1": body.damage1,
        ":damage2": body.damage2,
        ":damage3": body.damage3,
        ":damage4": body.damage4,
        ":damageDesc": body.damageDesc ? body.damageDesc : null,
        ":status1": body.status1,
        ":status2": body.status2,
        ":status3": body.status3,
        ":statusDesc": body.statusDesc ? body.statusDesc : null,
        ":imageUrls": body.imageUrls,
        ":shippingType": body.shippingType,
        ":price": body.price,
        ":location1": body.location1 ? body.location1 : null,
        ":location2": body.location2 ? body.location2 : null,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async updateTradeComplete(index: number) {
    const params = {
      TableName: "trade",
      Key: {
        index: index,
      },
      ExpressionAttributeNames: {
        "#complete": "complete",
        "#completeTimestamp": "completeTimestamp",
      },
      UpdateExpression:
        "set #complete = :complete, #completeTimestamp = :completeTimestamp",
      ExpressionAttributeValues: {
        ":complete": true,
        ":completeTimestamp": Date.now(),
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async updateTradePaidProgress(index: number, progress: number) {
    const params = {
      TableName: "trade",
      Key: {
        index: index,
      },
      ExpressionAttributeNames: {
        "#paidProgress": "paidProgress",
      },
      UpdateExpression: "set #paidProgress = :paidProgress",
      ExpressionAttributeValues: {
        ":paidProgress": progress,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async updateAdjustment(index: number) {
    const params = {
      TableName: "adjustment",
      Key: {
        index: index,
      },
      ExpressionAttributeNames: {
        "#complete": "complete",
      },
      UpdateExpression: "set #complete = :complete",
      ExpressionAttributeValues: {
        ":complete": true,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async shippingUpdate(
    index: number,
    code: string,
    num: number,
    request: string
  ) {
    const params = {
      TableName: "ask",
      Key: {
        index: index,
      },
      UpdateExpression:
        "set companyCode = :code, shippingNumber = :num, #request = :request",
      ExpressionAttributeNames: {
        "#request": "request",
      },
      ExpressionAttributeValues: {
        ":code": code,
        ":num": num,
        ":request": request,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }

  public async tradeStep(index: number) {
    const params = {
      TableName: "trade",
      Key: {
        index: index,
      },
      UpdateExpression: "set #step = :step",
      ExpressionAttributeNames: {
        "#step": "step",
      },
      ExpressionAttributeValues: {
        ":step": 1,
      },
      ReturnValues: "UPDATED_NEW",
    };

    const result = await db.update(params).promise();
    return result;
  }
}

export default DbMethod;
