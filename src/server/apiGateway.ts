import { fastify } from "../generic/interface";
import { multer } from "../generic/interface";
import { cors } from "../generic/interface";
import Axios from "axios";
import Auth from "../method/auth";
import Payment from "./paymentMethod";
import uuid from "uuid/v4";
import cryptoJs from "crypto-js";
import Crypto from "crypto";
import { Sneaker, Filter, WhiteList, Cheat } from "../store/reduce";
import DbMethod from "../database/dbMethod";
import ImageMethod from "./imageMethod";
import StreamData from "../store/stream";
import {
  searchWord,
  replaceWord,
  randomNumber,
  makeCode,
  shuffle,
} from "../method/common";
import Mailer from "../method/mailer";
import { method } from "lodash";

const sha512 = cryptoJs.SHA512;
const base64 = cryptoJs.enc.Base64;

const server = fastify({ logger: true });
// const server = fastify({logger: false});
server.register(cors);
server.register(multer.contentParser);

class ApiGateway {
  private port: number;
  private stream: StreamData<Sneaker, Filter, WhiteList, Cheat>;
  private dbMethod: DbMethod;
  private imageMethod: ImageMethod;

  constructor(_port: number) {
    this.port = _port;
    this.stream = new StreamData();
    this.dbMethod = new DbMethod();
    this.imageMethod = new ImageMethod();
  }

  private async initData() {
    this.recurrentSneakers();
    const filters: any = await this.dbMethod.scanFull("filters");
    this.stream.filters = filters.Items;
  }

  private async recurrentSneakers() {
    const result: any = await this.dbMethod.scanFull("sneakers");
    const sneakers = result.Items;

    for (let i in sneakers) {
      const tradeList = await this.dbMethod.queryPrice(sneakers[i].index);
      let arr: number[] = [];
      for (let k in tradeList) {
        arr.push(tradeList[k].price);
      }
      arr = arr.sort((a, b) => a - b);
      sneakers[i].lowPrice = arr[0];
      sneakers[i].tradeCount = arr.length;

      let arr2: number[] = [];
      for (let k in tradeList) {
        arr2.push(tradeList[k].index);
      }
      arr2 = arr2.sort((a, b) => b - a);
      sneakers[i].lastTradeTimestamp = arr2[0];
    }

    sneakers.sort((a: any, b: any) => {
      if (a.releaseDate > b.releaseDate) {
        return -1;
      }
      if (a.releaseDate < b.releaseDate) {
        return 1;
      }
      return 0;
    });

    this.stream.sneakers = sneakers;
  }

  private get(): void {
    server.get("/", async (request: any, reply: any) => {
      console.log("!@#!@#!@#!@#!@#!@")
      reply.code(200);
      return {message: "success", result: "result state successddddds!!"};
    });

    server.get("/stream", async (request: any, reply: any) => {
      console.log("!@#@!3122131");
      reply.code(200);
      return {message: "success", result: this.stream.sneakers};
    });

    server.get("/recurrent", async (request: any, reply: any) => {
      this.stream.recurrentStream([request.query]);
      reply.code(200);
      return {message: "success", result: "recurrent"};
    });
  }

  private userNotification(): void {
    server.post("/auth/getUserNotificationByUUID", async(request: any, reply: any)=>{
      var result = await this.dbMethod.getUserNotificationByUUID({UUID: request.body.UUID})

      return {message: "success", notification: result.Items[0].notification};
    })

    server.post("/users", async (request: any, reply: any) => {
      const result = await this.dbMethod.getUserList();
      return {messate: "success", userList: result};
    });

    server.post("/auth/getUserNotification", async (request: any, reply: any) => {
      reply.code(200);

      let decode: any;

      try {
        decode = Auth.decodeToken(request.body.token);
      } catch (error) {
        console.error(error);
        return {message: "failed get user token", result: "failed"};
      }

      const result = await this.dbMethod.getUserInformation({
        index: decode.index
      });

      return {message: "success", notification: result.Item?.notification};
    });

    server.post("/auth/updateUserNotificationTrue", async (request: any, reply: any) => {
      reply.code(200);

      let decode: any;

      try {
        decode = Auth.decodeToken(request.body.token);
      } catch (error) {
        console.error(error);
        return {message: "failed get user token", result: "failed"};
      }

      await this.dbMethod.setUserNotificationTrue({
        index: decode.index,
      });

      return {message: "user notification is setted true"};
    });

    server.post("/auth/updateUserNotificationFalse", async (request: any, reply: any) => {
      reply.code(200);

      let decode: any;

      try {
        decode = Auth.decodeToken(request.body.token);
      } catch (error) {
        console.error(error);
        return {message: "failed get user token", result: "failed"};
      }

      await this.dbMethod.setUserNotificationFalse({
        index: decode.index,
      });

      return {message: "user notification is setted false"};
    });

    server.post("/auth/saveUserDeviceToken", async (request: any, reply: any) => {
      let decode: any;
      try {
        decode = Auth.decodeToken(request.body.token);
      } catch (error) {
        console.log(error);
        return {message: "failed to get user Token", result: "failed"};
      }

      const result = await this.dbMethod.saveUserDeviceToken({
        deviceToken: request.body.deviceToken,
        index: decode.index
      });

      return {message: "success", result: result};

    });

    server.post("/auth/getUserDeviceToken", async (request: any, reply: any) => {
      let decode: any;

      try {
        decode = Auth.decodeToken(request.body.token);
      } catch (error) {
        console.log(error);
        return {message: "failed to get user Token", result: "failed"};
      }

      const result = await this.dbMethod.getUserInformation({
        index: decode.index
      });

      return {message: "success", deviceToken: result.Item?.deviceToken};
    });
  }

  private scanLocal(): void {
    server.get("/data/main", async (request: any, reply: any) => {
      const arr: Sneaker[] = this.stream.sneakers;

      arr.sort((a: any, b: any) => {
        if (a.tradeCount > b.tradeCount) {
          return -1;
        }
        if (a.tradeCount < b.tradeCount) {
          return 1;
        }
        return 0;
      }); // sort by release date...

      reply.code(200);
      return {message: "faild", result: arr.slice(0, 12)}; // except test
    }); // return string name

    server.get("/data/sneakers", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const objectFilter: any = JSON.parse(request.query.filter);
        const filter: any = Object.keys(objectFilter).map(
            (i: any) => objectFilter[i]
        );
        const sort: string = request.query.sort;
        const length: number = parseInt(request.query.length);
        const sneakers: any = this.stream.sneakers;

        if (sort === "최근 발매일순") {
          sneakers.sort((a: any, b: any) => {
            if (a.releaseDate > b.releaseDate) {
              return -1;
            }
            if (a.releaseDate < b.releaseDate) {
              return 1;
            }
            return 0;
          });
        }

        if (sort === "인기순") {
          sneakers.sort((a: any, b: any) => {
            if (a.tradeCount > b.tradeCount) {
              return -1;
            }
            if (a.tradeCount < b.tradeCount) {
              return 1;
            }
            return 0;
          });
        }

        if (sort === "최근 거래순") {
          sneakers.sort((a: any, b: any) => {
            if (a.lastTradeTimestamp > b.lastTradeTimestamp) {
              return -1;
            }
            if (a.lastTradeTimestamp < b.lastTradeTimestamp) {
              return 1;
            }
            return 0;
          });
        }

        if (filter.length === 0)
          return {message: "success", result: sneakers.slice(0, length)};

        const arr: Sneaker[] = [];
        for (let i in filter) {
          for (let k in sneakers) {
            if (sneakers[k].brand.toUpperCase() === filter[i]) {
              arr.push(sneakers[k]);
            }
          }
        }

        return {message: "success", result: arr.slice(0, length)};
      } catch (error) {
        return {message: "faild", result: []};
      }
    });

    server.get("/data/sneaker/name", async (request: any, reply: any) => {
      for (let i = 0; i < this.stream.sneakers.length; ++i) {
        if (this.stream.sneakers[i].name === request.query.name) {
          const result: Sneaker = this.stream.sneakers[i];
          let recommend: Sneaker[] = this.stream.sneakers.filter(
              (sneaker: Sneaker) =>
                  searchWord(sneaker.brand).includes(searchWord(result.brand))
          );
          recommend = shuffle(recommend);

          reply.code(200);
          return {
            message: "success",
            result: result,
            recommend: recommend.slice(0, 4),
          };
        }
      }

      reply.code(200);
      return {message: "faild", result: "not exist"};
    }); // return string name

    server.get("/data/sneaker/index", async (request: any, reply: any) => {
      reply.code(200);

      for (let i = 0; i < this.stream.sneakers.length; ++i) {
        if (this.stream.sneakers[i].index === parseInt(request.query.index)) {
          const result: Sneaker = this.stream.sneakers[i];
          return {message: "success", result: result};
        }
      }

      return {message: "faild", result: "not exist"};
    });

    server.get("/search/brand", async (request: any, reply: any) => {
      const result: Sneaker[] = this.stream.sneakers.filter(
          (sneaker: Sneaker) =>
              searchWord(sneaker.brand).includes(searchWord(request.query.search))
      );

      reply.code(200);
      return {message: "success", result: result};
    });

    server.get("/search/sneakers", async (request: any, reply: any) => {
      const result: Sneaker[] = this.stream.sneakers.filter(
          (sneaker: Sneaker) =>
              searchWord(sneaker.nameKr).includes(
                  searchWord(request.query.search)
              ) ||
              searchWord(sneaker.name).includes(searchWord(request.query.search)) ||
              searchWord(sneaker.brand).includes(searchWord(request.query.search))
      );

      reply.code(200);
      return {message: "success", result: result};
    });

    server.get("/data/filters", async (request: any, reply: any) => {
      const filters: Filter[] = this.stream.filters;

      reply.code(200);
      return {message: "success", result: filters};
    });
  }

  private price(): void {
    server.get("/trade/price", async (request: any, reply: any) => {
      const result = await this.dbMethod.queryPrice(
          parseInt(request.query.index)
      );
      let arr: number[] = [];
      for (let i in result) {
        arr.push(result[i].price);
      }

      arr = arr.sort((a, b) => a - b);

      reply.code(200);
      return {message: "success", result: arr[0]};
    });

    server.get("/trade/pricelist", async (request: any, reply: any) => {
      const sneakerIndex: number = parseInt(request.query.index);
      const result = await this.dbMethod.queryTradePrice(sneakerIndex);

      reply.code(200);
      return {message: "success", result: result};
    });
  }

  private trade(): void {
    server.route({
      method: "POST",
      url: "/trade/imageupload",
      preHandler: this.imageMethod.memoryStorage,
      handler: async (request: any, reply: any) => {
        const arr = [];

        for (let i = 0; i < request.files.length; ++i) {
          const result = await this.imageMethod.uploadTrade(request.files[i]);
          arr.push(result.Location);
        }

        reply.code(200);
        return {message: "success", result: arr};
      },
    });

    server.post("/trade/upload", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.body.token);
        const data = request.body;
        delete data.token;
        data.seller = decode.index;
        const timestamp = Date.now();
        data.index = timestamp;
        data.sort = timestamp;
        data.complete = false;
        data.sneakerIndex = parseInt(data.sneakerIndex);
        data.step = 0;
        if (data.damageDesc === "") delete data.damageDesc;
        if (data.statusDesc === "") delete data.statusDesc;
        if (data.location2 === "") delete data.location2;

        await this.dbMethod.put("trade", data);
        this.recurrentSneakers();
        return {message: "success", result: timestamp};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.get("/trade/list", async (request: any, reply: any) => {
      const index: number = parseInt(request.query.index);
      const result = await this.dbMethod.queryTradeList(
          index,
          request.query.limit
      );
      reply.code(200);

      return {message: "success", result: result};
    });

    server.get("/trade/detail", async (request: any, reply: any) => {
      const index: number = parseInt(request.query.index);
      let trade: any = await this.dbMethod.getByIndex("trade", index);
      console.log(trade);
      trade = trade.Item;
      const sellerIndex: number = parseInt(trade.seller);
      const user: any = await this.dbMethod.getUser(sellerIndex);
      trade.nickname = user.Item.nickname;
      trade.verifyPhone = user.Item.userVerify;
      reply.code(200);
      return {message: "success", result: trade};
    });

    server.get("/trade/block", async (request: any, reply: any) => {
      const exist: any = await this.dbMethod.scanAskMyExist(
          parseInt(request.query.trade),
          parseInt(request.query.index)
      );

      reply.code(200);
      return {message: "success", result: exist};
    });

    server.post("/trade/edit", async (request: any, reply: any) => {
      reply.code(200);

      try {
        // const decode: any = Auth.decodeToken(request.body.token);
        const data = request.body;
        if (data.damageDesc === "") delete data.damageDesc;
        if (data.statusDesc === "") delete data.statusDesc;
        if (data.location1 === "") delete data.location1;
        if (data.location2 === "") delete data.location2;

        await this.dbMethod.editTrade(data);
        this.recurrentSneakers();
        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.post("/trade/refuse", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const body: any = request.body;
        const decode: any = Auth.decodeToken(body.token);
        delete body.token;
        let user: any = await this.dbMethod.getByIndex("users", decode.index);
        user = user.Item;
        Mailer.sendEmail(
            "거래파기요청",
            `
        <p>user index: ${decode.index}</p>
        <p>user phone: ${user.phone}</p>
        <p>user email: ${user.email}</p>
        <p>index: ${body.index}</p>
        <p>종류: ${body.cancleType}</p>
        <p>이유: ${body.cancleDesc}</p>`
        );

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.post("/trade/update", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const body: any = request.body;
        const decode: any = Auth.decodeToken(body.token);
        delete body.token;
        let user: any = await this.dbMethod.getByIndex("users", decode.index);
        user = user.Item;

        const data: any = {
          index: Date.now(),
          sellerIndex: user.index,
          nickname: user.nickname,
          name: user.name,
          phone: user.phone,
          bank: user.bank.bankName,
          account: user.bank.account,
          owner: user.bank.owner,
          price: body.amount,
          tradeIndex: body.tradeIndex,
          tradeName: body.tradeName,
          tradeSize: body.tradeSize,
          smsAgree: body.smsAgree,
          complete: false,
        };

        this.dbMethod.updateTradePaidProgress(body.tradeIndex, 1);
        this.dbMethod.put("adjustment", data);

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });
  }

  private trading() {
    server.get("/trading/buyer", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        const tradings: any = await this.dbMethod.scanBuyer(
            "ask",
            decode.index
        );

        for (let i in tradings) {
          const imageUrl: any = await this.dbMethod.queryTradeIndex(
              tradings[i].tradeIndex
          );
          tradings[i].imageUrl = imageUrl;
          const completeTimestamp: any = await this.dbMethod.queryTradeIndexCompleteTimestamp(
              tradings[i].tradeIndex
          );
          if (completeTimestamp) {
            tradings[i].completeTimestamp = completeTimestamp;
          } else {
            tradings[i].completeTimestamp = 0;
          }
        }

        return {message: "success", result: tradings};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.get("/trading/seller", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        const tradings: any = await this.dbMethod.scanSeller(
            parseInt(decode.index)
        ); // can config

        for (let i in tradings) {
          try {
            const sneakerData: any = await this.dbMethod.querySneakerIndex(
                parseInt(tradings[i].sneakerIndex)
            );
            tradings[i].brand = sneakerData.brand;
            tradings[i].name = sneakerData.name;
            tradings[i].isTrading = false;
          } catch (error) {
          }
        }

        return {message: "success", result: tradings};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    }); // get sellers sneaker item

    server.get("/trading/detail", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        let ask: any = await this.dbMethod.getByIndex(
            "ask",
            parseInt(request.query.index)
        );
        ask = ask.Item;
        // console.log(ask);
        // if (decode.index !== ask.seller && decode.index !== ask.buyer) return { message: "faild token", result: "faild" };

        let trade: any = await this.dbMethod.getByIndex(
            "trade",
            ask.tradeIndex
        );
        trade = trade.Item;
        let sneaker: any = await this.dbMethod.getByIndex(
            "sneakers",
            trade.sneakerIndex
        );
        sneaker = sneaker.Item;
        const seller: any = await this.dbMethod.getByIndex(
            "users",
            trade.seller
        );
        const buyer: any = await this.dbMethod.getByIndex("users", ask.buyer);
        const data = {
          ...ask,
          ...trade,
          ...sneaker,
          askIndex: ask.index,
          seller: {
            name: seller.Item.name,
            phone: seller.Item.phone,
            email: seller.Item.email,
          },
          buyer: {
            name: buyer.Item.name,
            phone: buyer.Item.phone,
            email: buyer.Item.email,
          },
        };

        return {message: "success", result: data};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    }); // get sellers sneaker item
  }

  private ask() {
    server.post("/ask/put", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.body.token);
        const data = request.body;
        delete data.token;
        data.buyer = decode.index;
        const timestamp = Date.now();
        data.index = timestamp;
        data.sort = timestamp;
        data.complete = false;

        const seller: any = await this.dbMethod.getUser(parseInt(data.seller));
        Auth.smsSend(
            seller.Item.phone,
            "[ PAIR2 ] 거래요청 알림",
            `제품명 : ${data.sneakerName}\n사이즈 : ${data.size}\n\n판매중인 위 제품에 대한 거래요청이 있습니다. 판매내역에서 거래 수락/거절 여부를 결정해 주세요.`
        );

        const result = await this.dbMethod.put("ask", data);
        return {message: "success", result: result};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.get("/ask/seller", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        const ask: any = await this.dbMethod.scanSellerAsk(decode.index);

        for (let i in ask) {
          const imageUrl: any = await this.dbMethod.queryTradeIndex(
              ask[i].tradeIndex
          );
          ask[i].imageUrl = imageUrl;
          const completeTimestamp: any = await this.dbMethod.queryTradeIndexCompleteTimestamp(
              ask[i].tradeIndex
          );
          if (completeTimestamp) {
            ask[i].completeTimestamp = completeTimestamp;
          } else {
            ask[i].completeTimestamp = 0;
          }
        }

        return {message: "success", result: ask};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    }); // get sellers sneaker item

    server.get("/ask/exist", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const tradeIndex: number = parseInt(request.query.index);
        const existCount: any = await this.dbMethod.scanAskExist(tradeIndex);

        return {message: "success", result: existCount};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    }); // asking exsit check

    server.get("/ask/buyerinfo", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const index: number = parseInt(request.query.index);
        const result: any = await this.dbMethod.getAsk(index);
        const user: any = await this.dbMethod.getUser(result.buyer);
        result.nickname = user.Item.nickname;

        return {message: "success", result: result};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.get("/ask/refuse", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const index: number = parseInt(request.query.index);
        await this.dbMethod.editAsk(index, "거래거절");
        const ask: any = await this.dbMethod.getByIndex("ask", index);
        const buyer: any = await this.dbMethod.getUser(
            parseInt(ask.Item.buyer)
        );
        Auth.smsSend(
            buyer.Item.phone,
            "[ PAIR2 ] 거래거절 알림",
            `제품명 : ${ask.Item.sneakerName}\n사이즈 : ${ask.Item.size}\n\n안타깝지만 판매자의 사정으로 거래가 거절되었습니다. 다른 매물을 찾아보시겠어요?`
        );

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    }); // refuse only one

    server.get("/ask/accept", async (request: any, reply: any) => {
      try {
        const decode: any = Auth.decodeToken(request.query.token);
        const seller: number = parseInt(decode.index);
        const index = parseInt(request.query.index);
        const asks: any = await this.dbMethod.scanSellerAsk(seller);
        let result: any = "";
        let tradeIndex: number = 0;

        for (let i in asks) {
          if (asks[i].index === index) tradeIndex = asks[i].tradeIndex;
        }

        for (let i in asks) {
          if (
              asks[i].index === index &&
              asks[i].isShipping &&
              asks[i].status === "거래요청중"
          ) {
            result = await this.paymentAsk(
                asks[i].index,
                asks[i].buyer,
                asks[i].price
            );
            if (result.result !== "pay faild") {
              await this.dbMethod.editAsk(asks[i].index, "배송준비중");
              this.dbMethod.tradeStep(asks[i].tradeIndex);

              const buyer: any = await this.dbMethod.getUser(
                  parseInt(asks[i].buyer)
              );
              Auth.smsSend(
                  buyer.Item.phone,
                  "[ PAIR2 ] 거래수락 알림",
                  `제품명 : ${asks[i].sneakerName}\n사이즈 : ${asks[i].size}\n\n거래요청한 위 제품을 판매자가 거래수락하였습니다. 구매내역에서 판매자의 연락처를 확인할 수 있습니다. 결제 대금은 거래완료 될 때까지 안전하게 보관되오니 거래를 진행하세요.`
              );
            }
          } else if (
              asks[i].index === index &&
              !asks[i].isShipping &&
              asks[i].status === "거래요청중"
          ) {
            result = await this.paymentAsk(
                asks[i].index,
                asks[i].buyer,
                asks[i].price
            );
            if (result.result !== "pay faild") {
              await this.dbMethod.editAsk(asks[i].index, "직거래대기");
              this.dbMethod.tradeStep(asks[i].tradeIndex);

              const buyer: any = await this.dbMethod.getUser(
                  parseInt(asks[i].buyer)
              );
              Auth.smsSend(
                  buyer.Item.phone,
                  "[ PAIR2 ] 거래수락 알림",
                  `제품명 : ${asks[i].sneakerName}\n사이즈 : ${asks[i].size}\n\n거래요청한 위 제품을 판매자가 거래수락하였습니다. 구매내역에서 판매자의 연락처를 확인할 수 있습니다. 결제 대금은 거래완료 될 때까지 안전하게 보관되오니 거래를 진행하세요.`
              );
            }
          } else {
            if (
                asks[i].tradeIndex === tradeIndex &&
                asks[i].status === "거래요청중"
            ) {
              await this.dbMethod.editAsk(asks[i].index, "거래거절"); // refuse all asking
              const buyer: any = await this.dbMethod.getUser(
                  parseInt(asks[i].buyer)
              );
              Auth.smsSend(
                  buyer.Item.phone,
                  "[ PAIR2 ] 거래거절 알림",
                  `제품명 : ${asks[i].sneakerName}\n사이즈 : ${asks[i].size}\n\n안타깝지만 판매자의 사정으로 거래가 거절되었습니다. 다른 매물을 찾아보시겠어요?`
              );
            }
          }
        }

        return {message: "success", result: result.result};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.get("/ask/update", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const index = parseInt(request.query.index);
        await this.dbMethod.editAsk(index, request.query.status);

        if (request.query.status === "거래완료") {
          const ask: any = await this.dbMethod.getByIndex("ask", index);
          const seller: any = await this.dbMethod.getUser(
              parseInt(ask.Item.seller)
          );
          const tradeIndex: number = parseInt(ask.Item.tradeIndex);
          await this.dbMethod.updateTradeComplete(tradeIndex);

          Auth.smsSend(
              seller.Item.phone,
              "[ PAIR2 ] 거래완료 알림",
              `제품명 : ${ask.Item.sneakerName}\n사이즈 : ${ask.Item.size}\n\n판매한 위 제품이 거래완료 되었습니다. 판매내역에서 정산을 신청할 수 있습니다.`
          );
        }

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.get("/ask/delete", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const index = parseInt(request.query.index);
        const decode: any = Auth.decodeToken(request.query.token);
        const ask: any = await this.dbMethod.getByIndex("ask", index);
        if (ask.Item.buyer === decode.index) this.dbMethod.delete("ask", index);

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });
  }

  private shipping(): void {
    server.get("/shipping/update", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const index: number = parseInt(request.query.index);
        const ask: any = await this.dbMethod.getByIndex("ask", index);
        const buyer: any = await this.dbMethod.getUser(
            parseInt(ask.Item.buyer)
        );

        Auth.smsSend(
            buyer.Item.phone,
            "[ PAIR2 ] 배송 알림",
            `제품명 : ${ask.Item.sneakerName}\n사이즈 : ${ask.Item.size}\n\n판매자가 위 제품을 발송하였습니다. 구매내역에서 배송조회를 할 수 있습니다.`
        );

        await this.dbMethod.shippingUpdate(
            index,
            request.query.code,
            request.query.num,
            request.query.request
        );
        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });

    server.get("/shipping/where", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const index: number = parseInt(request.query.index);
        const result: any = await this.dbMethod.getByIndex("ask", index);
        return {
          message: "success",
          result: {
            code: result.Item.companyCode,
            number: result.Item.shippingNumber,
          },
        };
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }
    });
  } // shipping update for client from mypage

  private admin(): void {
    server.route({
      method: "POST",
      url: "/admin/upload/image",
      preHandler: this.imageMethod.memoryStorage,
      handler: async (request: any, reply: any) => {
        const result = await this.imageMethod.upload(
            request.files[0],
            request.headers.size
        );

        reply.code(200);
        return {message: "success", result: result};
      },
    });

    server.route({
      method: "POST",
      url: "/admin/upload/imagerelease",
      preHandler: this.imageMethod.memoryStorage,
      handler: async (request: any, reply: any) => {
        const result = await this.imageMethod.uploadRelease(
            request.files[0],
            request.headers.size
        );

        reply.code(200);
        return {message: "success", result: result};
      },
    });

    server.get("/admin/data/sneakers", async (request: any, reply: any) => {
      reply.code(200);
      return {message: "success", result: this.stream.sneakers};
    });

    server.post("/admin/add/sneaker", async (request: any, reply: any) => {
      await this.dbMethod.put("sneakers", {
        index: Date.now(),
        brand: request.body.brand,
        name: request.body.name,
        nameKr: request.body.nameKr,
        dollar: request.body.dollar,
        releaseDate: request.body.releaseDate,
        releasePrice: request.body.releasePrice,
        serial: request.body.serial,
        thumbUrl: request.body.thumbUrl,
        imageUrl1: request.body.imageUrl1,
        imageUrl2: request.body.imageUrl2,
        imageUrl3: request.body.imageUrl3,
        imageUrl4: request.body.imageUrl4,
        imageUrl5: request.body.imageUrl5,
      });

      this.recurrentSneakers();
      reply.code(200);
      return {message: "success", result: "success!"};
    });

    server.post("/admin/edit/sneaker", async (request: any, reply: any) => {
      const result = await this.dbMethod.editSneaker(request.body);
      const sneakers: any = await this.dbMethod.scanFull("sneakers");
      this.stream.sneakers = sneakers.Items;
      this.recurrentSneakers();

      reply.code(200);
      return {message: "success", result: result};
    });

    server.get("/admin/delete/sneaker", async (request: any, reply: any) => {
      const result = await this.dbMethod.delete(
          "sneakers",
          parseInt(request.query.index)
      );
      const sneakers: any = await this.dbMethod.scanFull("sneakers");
      this.stream.sneakers = sneakers.Items;
      this.recurrentSneakers();
      reply.code(200);
      return {message: "success", result: "success"};
    });

    server.get("/admin/data/adjustment", async (request: any, reply: any) => {
      reply.code(200);
      const adjustment: any = await this.dbMethod.scanFull("adjustment");
      return {message: "success", result: adjustment.Items};
    });

    server.get("/admin/update/adjustment", async (request: any, reply: any) => {
      reply.code(200);
      this.dbMethod.updateTradePaidProgress(
          parseInt(request.query.tradeIndex),
          2
      );
      this.dbMethod.updateAdjustment(parseInt(request.query.adjustmentIndex));
      return {message: "success", result: "success"};
    });

    server.get("/admin/data/release", async (request: any, reply: any) => {
      const result: any = await this.dbMethod.scanFull("release");
      reply.code(200);
      return {message: "success", result: result.Items};
    });

    server.post("/admin/add/release", async (request: any, reply: any) => {
      await this.dbMethod.put("release", {
        index: Date.now(),
        brand: request.body.brand,
        name: request.body.name,
        nameKr: request.body.nameKr,
        price: request.body.price,
        color: request.body.color,
        serial: request.body.serial,
        unknownRelease: request.body.unknownRelease,
        releaseDate: request.body.releaseDate,
        imageUrl: request.body.imageUrl,
        imageUrlLarge: request.body.imageUrlLarge,
        body: request.body.body,
        marketPriceUrlKream: request.body.marketPriceUrlKream,
        marketPriceUrlStockx: request.body.marketPriceUrlStockx,
      });

      reply.code(200);
      return {message: "success", result: "success!"};
    });

    server.post("/admin/edit/release", async (request: any, reply: any) => {
      await this.dbMethod.put("release", {
        index: parseInt(request.body.index),
        brand: request.body.brand,
        name: request.body.name,
        nameKr: request.body.nameKr,
        price: request.body.price,
        color: request.body.color,
        serial: request.body.serial,
        unknownRelease: request.body.unknownRelease,
        releaseDate: request.body.releaseDate,
        imageUrl: request.body.imageUrl,
        imageUrlLarge: request.body.imageUrlLarge
            ? request.body.imageUrlLarge
            : "",
        marketPriceUrlKream: request.body.marketPriceUrlKream,
        marketPriceUrlStockx: request.body.marketPriceUrlStockx,
        body: request.body.body,
      });

      reply.code(200);
      return {message: "success", result: "success!"};
    });

    server.get("/admin/delete/release", async (request: any, reply: any) => {
      const result = await this.dbMethod.delete(
          "release",
          parseInt(request.query.index)
      );
      reply.code(200);
      return {message: "success", result: result};
    });

    server.post("/admin/put/store", async (request: any, reply: any) => {
      const result = await this.dbMethod.put("store", {
        index: Date.now(),
        release: parseInt(request.body.release),
        storename: request.body.storename,
        storesaleType: request.body.storesaleType,
        storeCountry: request.body.storeCountry,
        releaseDate: request.body.releaseDate,
        releaseDateEnd: request.body.releaseDateEnd,
        imageUrl: request.body.imageUrl,
        price: request.body.price,
        online: request.body.online,
        storeUrl: request.body.storeurl,
      });

      reply.code(200);
      return {message: "success", result: "success"};
    });

    server.post("/admin/edit/store", async (request: any, reply: any) => {
      const result = await this.dbMethod.put("store", {
        index: parseInt(request.body.index),
        release: parseInt(request.body.release),
        storename: request.body.storename,
        storesaleType: request.body.storesaleType,
        storeCountry: request.body.storeCountry,
        releaseDate: request.body.releaseDate,
        releaseDateEnd: request.body.releaseDateEnd,
        imageUrl: request.body.imageUrl,
        price: request.body.price,
        online: request.body.online,
        storeUrl: request.body.storeurl,
      });

      reply.code(200);
      return {message: "success", result: "success"};
    });

    server.get("/admin/delete/store", async (request: any, reply: any) => {
      const result = await this.dbMethod.delete(
          "store",
          parseInt(request.query.index)
      );
      reply.code(200);
      return {message: "success", result: result};
    });

    server.post("/admin/put/filter", async (request: any, reply: any) => {
      const result = await this.dbMethod.put("filters", {
        index: Date.now(),
        name: request.body.name,
        other: Boolean(request.body.other),
      });

      const filters: any = await this.dbMethod.scanFull("filters");
      this.stream.filters = filters.Items;
      reply.code(200);
      return {message: "success", result: result};
    });

    server.get("/admin/delete/filter", async (request: any, reply: any) => {
      const result = await this.dbMethod.delete(
          "filters",
          parseInt(request.query.index)
      );
      const filters: any = await this.dbMethod.scanFull("filters");
      this.stream.filters = filters.Items;
      this.recurrentSneakers();
      reply.code(200);
      return {message: "success", result: result};
    });

    server.post("/admin/add/faq", async (request: any, reply: any) => {
      const result = await this.dbMethod.put("faq", {
        index: Date.now(),
        title: request.body.title,
        category: request.body.category,
        desc: request.body.desc,
      });

      reply.code(200);
      return {message: "success", result: result};
    });

    server.get("/admin/put/commision", async (request: any, reply: any) => {
      const result = await this.dbMethod.put("config", {
        index: 1,
        commision: parseInt(request.query.commision),
        sale: parseInt(request.query.sale),
      });

      reply.code(200);
      return {message: "success", result: "success"};
    });

    server.get("/admin/get/trade", async (request: any, reply: any) => {
      let trade: any = await this.dbMethod.scanFull("trade");
      trade = trade.Items;

      for (let i in trade) {
        const sneaker = await this.dbMethod.getByIndex(
            "sneakers",
            trade[i].sneakerIndex
        );
        trade[i].sneaker = sneaker.Item;
        const seller = await this.dbMethod.getByIndex("users", trade[i].seller);
        trade[i].seller = seller.Item;
      }

      reply.code(200);
      return trade;
    });
  }

  private auth(): void {
    server.get("/auth/signin", async (request: any, reply: any) => {
      try {
        const userData: any = await this.dbMethod.scanUser(request.query.phone);
        const verify = Auth.verifyHash(
            request.query.password,
            userData.salt,
            userData.hash
        );

        reply.code(200);

        if (verify) {
          const token = Auth.newToken(userData.index);

          const refreshBody = {
            index: userData.index,
            deviceToken: request.query.deviceToken,
            UUID: request.query.UUID,
            notification: request.query.notification
          };

          await this.dbMethod.updateUserLogin(refreshBody);

          return {message: "success", token: token};
        } else {
          return {message: "faild", result: "faild"};
        }
      } catch (error) {
        console.log(error);
        reply.code(200);
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/auth/signinByEmail", async (request: any, reply: any) => {
      try {
        const userData: any = await this.dbMethod.scanUserByEmail(request.query.email);
        const verify = Auth.verifyHash(
            request.query.password,
            userData.salt,
            userData.hash
        );


        reply.code(200);

        if (verify) {
          const token = Auth.newToken(userData.index);

          const refreshBody = {
            index: userData.index,
            deviceToken: request.query.deviceToken,
            UUID: request.query.UUID,
            notification: request.query.notification
          };

          await this.dbMethod.updateUserLogin(refreshBody);

          return {message: "success", token: token};
        } else {
          return {message: "faild", result: "faild"};
        }
      } catch (error) {
        reply.code(200);
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/auth/smssend", async (request: any, reply: any) => {
      const exist: any = await this.dbMethod.scanItem(
          "users",
          "phone",
          request.query.phone
      );

      reply.code(200);

      if (exist.Count > 0) {
        return {message: "phone exist", result: "faild"};
      } else {
        const code: string = randomNumber();
        Auth.naverSms(request.query.phone, code);

        this.stream.whitelist.push({
          phone: request.query.phone,
          code: code,
          verify: false,
          timestamp: Date.now(),
        });
        // console.log(this.stream.whitelist);

        return {message: "success", result: "success"};
      }
    });

    server.get("/auth/smsverify", async (request: any, reply: any) => {
      const whitelist: WhiteList[] = [];
      let verify: boolean = false;

      for (let i = 0; i < this.stream.whitelist.length; ++i) {
        if (this.stream.whitelist[i].timestamp + 60000 * 5 > Date.now()) {
          if (
              this.stream.whitelist[i].phone === request.query.phone &&
              this.stream.whitelist[i].code === request.query.code
          ) {
            this.stream.whitelist[i].verify = true;
            verify = true;
          }
          whitelist.push(this.stream.whitelist[i]);
        }
      } // checking timestamp and verify match

      this.stream.whitelist = whitelist;

      reply.code(200);

      if (verify) {
        return {message: "success", result: "success"};
      } else {
        return {message: "faild verify phone", result: "faild"};
      }
    });

    server.post("/auth/signup", async (request: any, reply: any) => {
      let verifyPhone: boolean = false;
      const exist: any = await this.dbMethod.scanItem(
          "users",
          "phone",
          request.body.phone
      );
      // exist check email ... not now

      for (let i = 0; i < this.stream.whitelist.length; ++i) {
        if (
            this.stream.whitelist[i].timestamp + 60000 * 5 > Date.now() &&
            this.stream.whitelist[i].phone === request.body.phone &&
            this.stream.whitelist[i].code === request.body.code &&
            this.stream.whitelist[i].verify
        ) {
          verifyPhone = true;
        }
      }

      reply.code(200);

      if (!verifyPhone || exist.Count > 0) {
        return {message: "faild verify phone", result: "faild"};
      } else {
        const salt = uuid();
        const timestamp = Date.now(); // (index)
        const hash = sha512(request.body.password + salt).toString(base64);

        await this.dbMethod.put("users", {
          index: timestamp,
          hash: hash,
          salt: salt,
          name: request.body.name,
          phone: request.body.phone,
          nickname: request.body.nickname,
          email: request.body.email,
          age: request.body.age ? request.body.age : "없음",
          birthMonth: request.body.birthMonth ? request.body.birthMonth : 0,
          birthDate: request.body.birthDate ? request.body.birthDate : 0,
          sex: request.body.sex ? request.body.sex : "없음",
          size: request.body.size ? request.body.size : 0,
          smsAgree: request.body.smsAgree,
          emailAgree: request.body.emailAgree,
          agree1: request.body.agree1,
          agree2: request.body.agree2,
          agree3: request.body.agree3,
        });

        const token = Auth.newToken(timestamp);

        return {message: "signup success", result: token};
      }
    });

    server.post("/auth/signupByEmail", async (request: any, reply: any) => {

      const existEmail: any = await this.dbMethod.scanItem(
          "users",
          "email",
          request.body.email
      );

      reply.code(200);

      if (existEmail.Count > 0) {
        return {message: "Email existed.", result: "Email existed"};
      } else {
        const salt = uuid();
        const timestamp = Date.now(); // (index)
        const hash = sha512(request.body.password + salt).toString(base64);

        await this.dbMethod.put("users", {
          index: timestamp,
          hash: hash,
          salt: salt,
          name: request.body.name,
          phone: request.body.phone,
          nickname: request.body.nickname,
          email: request.body.email,
          age: request.body.age ? request.body.age : "없음",
          birthMonth: request.body.birthMonth ? request.body.birthMonth : 0,
          birthDate: request.body.birthDate ? request.body.birthDate : 0,
          sex: request.body.sex ? request.body.sex : "없음",
          size: request.body.size ? request.body.size : 0,
          smsAgree: request.body.smsAgree,
          emailAgree: request.body.emailAgree,
          agree1: request.body.agree1,
          agree2: request.body.agree2,
          agree3: request.body.agree3,
        });

        const token = Auth.newToken(timestamp);

        return {message: "signup success", result: token};
      }
    });

    server.post("/auth/edituser", async (request: any, reply: any) => {
      reply.code(200);

      let decode: any;

      try {
        decode = Auth.decodeToken(request.body.token);
      } catch (error) {
        return {message: "faild token", result: "faild"};
      }

      let verifyPhone: boolean = false;

      for (let i = 0; i < this.stream.whitelist.length; ++i) {
        if (
            this.stream.whitelist[i].timestamp + 60000 * 5 > Date.now() &&
            this.stream.whitelist[i].phone === request.body.phone &&
            this.stream.whitelist[i].code === request.body.code &&
            this.stream.whitelist[i].verify
        ) {
          verifyPhone = true;
        }
      }

      try {
        const editPhoneNumber = await this.dbMethod.getPhone(decode.index);
        if (editPhoneNumber === request.body.phone) verifyPhone = true;
      } catch (error) {
      }

      if (!verifyPhone) {
        return {message: "faild verify phone", result: "faild"};
      } else {
        const salt = uuid();
        const hash = sha512(request.body.password + salt).toString(base64);

        await this.dbMethod.editUser({
          index: decode.index,
          hash: hash,
          salt: salt,
          name: request.body.name,
          phone: request.body.phone,
          nickname: request.body.nickname,
          email: request.body.email,
          age: request.body.age ? request.body.age : "없음",
          birthMonth: request.body.birthMonth ? request.body.birthMonth : 0,
          birthDate: request.body.birthDate ? request.body.birthDate : 0,
          sex: request.body.sex ? request.body.sex : "없음",
          size: request.body.size ? request.body.size : 0,
          smsAgree: request.body.smsAgree,
          emailAgree: request.body.emailAgree,
          agree1: request.body.agree1,
          agree2: request.body.agree2,
          agree3: request.body.agree3,
        });

        const token = Auth.newToken(decode.index);

        return {message: "edituser success", result: token};
      }
    });

    server.post("/auth/edituserByEmail", async (request: any, reply: any) => {
      reply.code(200);

      console.log(request.body);

      let decode: any;

      try {
        decode = Auth.decodeToken(request.body.token);
      } catch (error) {
        console.error(error);
        return {message: "failed get user token", result: "failed"};
      }

      let verifyEmail: boolean = false;

      try {
        const editEmail = await this.dbMethod.getEmail(decode.index);
        if (editEmail === request.body.email) verifyEmail = true;
      } catch (error) {
        console.error(error);
        return {message: "failed verify user email", result: "failed"};
      }

      if (verifyEmail) {
        const salt = uuid();
        const hash = sha512(request.body.password + salt).toString(base64);

        await this.dbMethod.editUserNew({
          index: decode.index,
          hash: hash,
          salt: salt,
          nickname: request.body.nickname,
          email: request.body.email,
          agree1: request.body.agree1,
          agree2: request.body.agree2,
        });


        const token = Auth.newToken(decode.index);

        console.log("user edit success!!");

        return {message: "edit user success", result: token};
      } else {
        console.log("failed verify email: ", request.body.email);
        return {message: "failed verify email", result: "failed"};
      }
    });

    server.get("/auth/smssendfind", async (request: any, reply: any) => {
      try {
        const userData: any = await this.dbMethod.scanNameWithPhone(
            request.query.phone
        );

        reply.code(200);

        if (
            userData.phone === request.query.phone &&
            userData.name === request.query.name
        ) {
          const code: string = randomNumber();
          Auth.naverSms(request.query.phone, code);

          this.stream.findlist.push({
            phone: request.query.phone,
            code: code,
            verify: false,
            timestamp: Date.now(),
          });
          // console.log(this.stream.findlist);

          return {message: "success", result: "success"};
        } else {
          return {message: "not found", result: "faild"};
        }
      } catch (error) {
        reply.code(200);
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/auth/smsverifyfind", async (request: any, reply: any) => {
      const findlist: WhiteList[] = [];
      let verify: boolean = false;

      for (let i = 0; i < this.stream.findlist.length; ++i) {
        if (this.stream.findlist[i].timestamp + 60000 > Date.now()) {
          if (
              this.stream.findlist[i].phone === request.query.phone &&
              this.stream.findlist[i].code === request.query.code
          ) {
            this.stream.findlist[i].verify = true;
            verify = true;
          }
          findlist.push(this.stream.findlist[i]);
        }
      } // checking timestamp and verify match

      this.stream.findlist = findlist;

      reply.code(200);

      if (verify) {
        return {message: "success", result: "success"};
      } else {
        return {message: "faild verify phone", result: "faild"};
      }
    });

    server.get("/auth/editpassword", async (request: any, reply: any) => {
      let verify: boolean = false;

      for (let i = 0; i < this.stream.findlist.length; ++i) {
        if (
            this.stream.findlist[i].timestamp + 60000 * 5 > Date.now() &&
            this.stream.findlist[i].phone === request.query.phone &&
            this.stream.findlist[i].code === request.query.code &&
            this.stream.findlist[i].verify
        ) {
          verify = true;
        }
      } // checking timestamp and verify match

      reply.code(200);

      if (verify) {
        const index = await this.dbMethod.scanUserIndex(request.query.phone);
        const salt = uuid();
        const hash = sha512(request.query.password + salt).toString(base64);
        const result = await this.dbMethod.editPassword(index, hash, salt);

        return {message: "success", result: "success"};
      } else {
        return {message: "faild verify phone", result: "faild"};
      }
    });

    server.get("/auth/verifyupdate", async (request: any, reply: any) => {
      const decode: any = Auth.decodeToken(request.query.token);
      const index: number = parseInt(decode.index);
      await this.dbMethod.editUserVerify(index);
      return {message: "success", result: "success"};
    });

    server.get("/auth/userverify", async (request: any, reply: any) => {
      const decode: any = Auth.decodeToken(request.query.token);
      const index: number = parseInt(decode.index);
      const userInfo: any = await this.dbMethod.getByIndex("users", index);
      return {message: "success", result: userInfo.Item.userVerify};
    });
  }

  private mypage(): void {
    server.get("/verify/mypage", async (request: any, reply: any) => {
      const decode: any = Auth.decodeToken(request.query.token);

      reply.code(200);

      if (decode) {
        const result = await this.dbMethod.getUser(decode.index);
        return {message: "success", result: result.Item};
      } else {
        return {message: "faild", result: "faild"}; // need new login
      }
    });

    server.get("/verify/userinfo", async (request: any, reply: any) => {
      const decode: any = Auth.decodeToken(request.query.token);

      reply.code(200);

      if (decode) {
        const result = await this.dbMethod.getByIndex("users", decode.index);
        return {message: "success", result: result.Item};
      } else {
        return {message: "faild", result: "faild"}; // need new login
      }
    });

    server.get("/verify/shippings", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        const result = await this.dbMethod.getShippings(decode.index);

        return {message: "success", result: result.shippings};
      } catch (error) {
        return {message: "faild", result: "faild"}; // need new login
      }
    });

    server.post("/verify/editshippings", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.body.token);
        const result = await this.dbMethod.editShippings(
            decode.index,
            request.body.shippings
        );

        return {message: "success", result: result};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/verify/bank", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        const result = await this.dbMethod.getBank(decode.index);
        return {message: "success", result: result.bank};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.post("/verify/editbank", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.body.token);
        const result = await this.dbMethod.editBank(
            decode.index,
            request.body.bank
        );

        return {message: "success", result: result};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });
  }

  private like(): void {
    server.post("/verify/putlike", async (request: any, reply: any) => {
      reply.code(200);

      const decode: any = Auth.decodeToken(request.body.token);
      if (!decode) return {message: "faild", result: "faild"};

      try {
        const exist = await this.dbMethod.queryExistLikes(
            decode.index,
            request.body.data
        );

        if (!exist) {
          await this.dbMethod.put("likes", {
            index: Date.now(),
            userSort: Date.now(),
            user: decode.index,
            ...request.body.data,
          });

          return {message: "success", result: "success"};
        } else {
          return {message: "success", result: "exist"};
        }
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.post("/verify/putliketrade", async (request: any, reply: any) => {
      reply.code(200);

      const decode: any = Auth.decodeToken(request.body.token);
      if (!decode) return {message: "faild", result: "faild"};

      try {
        const exist: any = await this.dbMethod.queryExistLikesTrade(
            decode.index,
            request.body.tradeIndex
        );
        if (!exist) {
          await this.dbMethod.put("likestrade", {
            index: Date.now(),
            sort: Date.now(),
            user: decode.index,
            tradeIndex: request.body.tradeIndex,
          });
        }

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/verify/likes", async (request: any, reply: any) => {
      reply.code(200);

      const decode: any = Auth.decodeToken(request.query.token);
      if (!decode) return {message: "faild", result: "faild"};

      try {
        const likes = await this.dbMethod.queryMyLikes(
            decode.index,
            request.query.count
        );

        return {
          message: "success",
          result: likes.slice(0, request.query.count),
        };
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/verify/likestrade", async (request: any, reply: any) => {
      reply.code(200);

      const decode: any = Auth.decodeToken(request.query.token);
      if (!decode) return {message: "faild", result: "faild"};

      try {
        const likes = await this.dbMethod.queryMyLikesTrade(
            decode.index,
            request.query.count
        );
        const result = [];

        for (let i in likes) {
          let tradeDetails: any = await this.dbMethod.getByIndex(
              "trade",
              likes[i].tradeIndex
          );
          tradeDetails = tradeDetails.Item;

          for (let k in this.stream.sneakers) {
            if (tradeDetails.sneakerIndex === this.stream.sneakers[k].index) {
              let condition: string = "Perfect";
              if (
                  tradeDetails.damage1 ||
                  tradeDetails.damage2 ||
                  tradeDetails.damage3 ||
                  tradeDetails.damage4
              )
                condition = "Defect1";
              if (condition === "Defect1") {
                if (
                    tradeDetails.status1 ||
                    tradeDetails.status2 ||
                    tradeDetails.status3
                )
                  condition = "Defect3";
              } else {
                if (
                    tradeDetails.status1 ||
                    tradeDetails.status2 ||
                    tradeDetails.status3
                )
                  condition = "Defect2";
              }

              const data = {
                tradeIndex: tradeDetails.index,
                sneakerName: this.stream.sneakers[k].name,
                size: tradeDetails.size,
                imageUrl: tradeDetails.imageUrls[0],
                price: tradeDetails.price,
                from: tradeDetails.from,
                status: condition,
                shippingType: tradeDetails.shippingType,
                likeIndex: likes[i].index,
              };

              result.push(data);
            }
          }
        }

        return {message: "success", result: result};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/verify/existlike", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        if (!decode) return {message: "faild", result: "faild"};
        const exist = await this.dbMethod.queryExistLikeName(
            decode.index,
            request.query.name
        );

        if (exist) {
          return {message: "success", result: "true"};
        } else {
          return {message: "faild", result: "faild"};
        }
      } catch (error) {
        return {message: "faild", result: "try faild"};
      }
    });

    server.get("/verify/existliketrade", async (request: any, reply: any) => {
      reply.code(200);
      try {
        const decode: any = Auth.decodeToken(request.query.token);
        if (!decode) return {message: "faild", result: "faild"};
        const exist: any = await this.dbMethod.queryExistLikesTrade(
            decode.index,
            parseInt(request.query.tradeindex)
        );

        if (exist === true) {
          return {message: "success", result: "true"};
        } else {
          return {message: "faild", result: "faild"};
        }
      } catch (error) {
        return {message: "faild", result: false};
      }
    });

    server.get("/verify/deletelike", async (request: any, reply: any) => {
      reply.code(200);

      const decode: any = Auth.decodeToken(request.query.token);
      if (!decode) return {message: "faild", result: "faild"};

      try {
        const result = await this.dbMethod.delete(
            "likes",
            parseInt(request.query.index)
        );

        return {message: "success", result: result};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/verify/deleteliketrade", async (request: any, reply: any) => {
      reply.code(200);

      const decode: any = Auth.decodeToken(request.query.token);
      if (!decode) return {message: "faild", result: "faild"};

      try {
        const result = await this.dbMethod.delete(
            "likestrade",
            parseInt(request.query.index)
        );
        return {message: "success", result: result};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });
  }

  private payment(): void {
    server.get("/payment/userindex", async (request: any, reply: any) => {
      reply.code(200);

      try {
        const decode: any = Auth.decodeToken(request.query.token);
        return {message: "success", result: decode.index};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.post("/payment/gateway", async (request: any, reply: any) => {
      reply.code(200);

      const decode: any = Auth.decodeToken(request.body.token);
      if (!decode) return {message: "faild", result: "falid token"};

      try {
        // const userInfo: any = await this.dbMethod.getByIndex("users", decode.index);
        const customer_uid = decode.index;
        const name = "거래";

        const paymentToken = await Payment.getToken();
        const result = await Payment.gateway(paymentToken, {
          customer_uid: customer_uid,
          merchant_uid: Date.now(),
          amount: request.body.price,
          name: name,
        });

        if (result.status === "paid") {
          return {message: "success", result: "success"};
        } else {
          return {message: "faild", result: "faild paid"}; // 결제 코드 실패...
        }
      } catch (error) {
        return {message: "faild", result: "faild try"};
      }
    });

    server.get("/payment/cardname", async (request: any, reply: any) => {
      reply.code(200);
      const decode: any = Auth.decodeToken(request.query.token);
      if (!decode) return {message: "faild", result: "faild"};

      try {
        const result = await this.dbMethod.getPayment(decode.index);
        return {
          message: "success",
          result: {
            cardName: result.card_name,
          },
        };
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.post("/payment/addcard", async (request: any, reply: any) => {
      reply.code(200);
      try {
        const decode: any = Auth.decodeToken(request.body.token);
        const data = request.body;
        delete data.token;
        await this.dbMethod.editPayment(decode.index, {...data});

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/payment/deletecard", async (request: any, reply: any) => {
      reply.code(200);
      try {
        const decode: any = Auth.decodeToken(request.query.token);
        await this.dbMethod.editPayment(decode.index, null);

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });
  }

  private etc(): void {
    server.post("/question/send", async (request: any, reply: any) => {
      reply.code(200);
      try {
        Mailer.sendEmail(
            request.body.type,
            `
        <p>종류: ${request.body.type}</p>
        <p>제목: ${request.body.title}</p>
        <p>내용: ${request.body.desc}</p>
        <p>이름: ${request.body.name}</p>
        <p>전화번호: ${request.body.phone}</p>
        <p>문자알람: ${request.body.questionAlert}</p>
        <p>이메일: ${request.body.email}</p>`
        );

        return {message: "success", result: "success"};
      } catch (error) {
        return {message: "faild", result: "faild"};
      }
    });

    server.get("/data/release", async (request: any, reply: any) => {
      const result: any = await this.dbMethod.scanFull("release");
      reply.code(200);
      return {message: "success", result: result.Items};
    });

    server.get("/data/release/today", async (request: any, reply: any) => {
      const result: any = await this.dbMethod.queryTodaybyStore();
      reply.code(200);
      return {message: "success", result: result};
    });

    server.get("/data/store", async (request: any, reply: any) => {
      const result: any = await this.dbMethod.queryStore(
          parseInt(request.query.index)
      );
      reply.code(200);

      // console.log("---------------------------------");
      // console.log(result);

      return {message: "success", result: result};
    });

    server.get("/data/release/detail", async (request: any, reply: any) => {
      const result: any = await this.dbMethod.getByIndex(
          "release",
          parseInt(request.query.index)
      );
      reply.code(200);
      return {message: "success", result: result.Item};
    });

    // server.get("/data/faq", async (request: any, reply: any) => {
    //   const result: any = await this.dbMethod.scanFull("faq");
    //   reply.code(200);
    //   return { message: "faild", result: result.Items };
    // });

    // server.get("/data/cheat", async (request: any, reply: any) => {
    //   const phone: string = request.query.phone;
    //   let exist: boolean = false;
    //   let cheat: boolean = false;

    //   for (const i in this.stream.cheatlist) {
    //     if (this.stream.cheatlist[i].phone === phone) exist = true;
    //     if (
    //       this.stream.cheatlist[i].phone === phone &&
    //       this.stream.cheatlist[i].cheat === true
    //     )
    //       cheat = true;
    //   }

    //   if (!exist) {
    //     const result = await this.cheat(phone);
    //     if (result === "N") {
    //       this.stream.cheatlist.push({ phone: phone, cheat: false });
    //     } else {
    //       this.stream.cheatlist.push({ phone: phone, cheat: true });
    //       cheat = true;
    //     }
    //   } // recurrent push

    //   reply.code(200);
    //   return { message: "success", result: cheat };
    // });

    // server.get("/data/cheatindex", async (request: any, reply: any) => {
    //   const index: number = parseInt(request.query.index);
    //   const user: any = await this.dbMethod.getByIndex("users", index);
    //   const phone: string = user.Item.phone;
    //   let exist: boolean = false;
    //   let cheat: boolean = false;

    //   for (const i in this.stream.cheatlist) {
    //     if (this.stream.cheatlist[i].phone === phone) exist = true;
    //     if (
    //       this.stream.cheatlist[i].phone === phone &&
    //       this.stream.cheatlist[i].cheat === true
    //     )
    //       cheat = true;
    //   }

    //   if (!exist) {
    //     const result = await this.cheat(phone);
    //     if (result === "N") {
    //       this.stream.cheatlist.push({ phone: phone, cheat: false });
    //     } else {
    //       this.stream.cheatlist.push({ phone: phone, cheat: true });
    //       cheat = true;
    //     }
    //   } // recurrent push

    //   reply.code(200);
    //   return { message: "success", result: cheat };
    // });

    // server.get("/data/cheatrecurrent", async (request: any, reply: any) => {
    //   const phone: string = request.query.phone;
    //   let exist: boolean = false;
    //   const result = await this.cheat(request.query.phone);

    //   for (const i in this.stream.cheatlist) {
    //     if (this.stream.cheatlist[i].phone === phone) exist = true;
    //   }

    //   if (!exist) {
    //     if (result === "N") {
    //       this.stream.cheatlist.push({ phone: phone, cheat: false });
    //     } else {
    //       this.stream.cheatlist.push({ phone: phone, cheat: true });
    //     }
    //   } else {
    //     if (result === "N") {
    //       for (const i in this.stream.cheatlist) {
    //         if (this.stream.cheatlist[i].phone === phone)
    //           this.stream.cheatlist[i].cheat = false;
    //       }
    //     } else {
    //       for (const i in this.stream.cheatlist) {
    //         if (this.stream.cheatlist[i].phone === phone)
    //           this.stream.cheatlist[i].cheat = true;
    //       }
    //     }
    //   }

    //   reply.code(200);
    //   return { message: "success", result: "recurrented" };
    // });

    // server.get("/test/cheat", async (request: any, reply: any) => {
    //   const { data } = await Axios.get(
    //     `http://13.209.92.132:8081/search/cheat?phone=${
    //       request.query.phone
    //     }&key=${"pair2apikey"}`
    //   );
    //   const content = data.data.content;
    //   const decode = Buffer.from(content, "base64").toString();
    //   const split = decode.split(",").join("");
    //   const hex2bin = Buffer.from(split, "hex").toString();
    //   const secret: string = "7BZi5T90#ul6a!UzsZ9eViLF*9s9M1Th";
    //   const cipher = Crypto.createDecipheriv(
    //     "aes-256-cbc",
    //     secret,
    //     secret.slice(0, 16)
    //   );
    //   let decrypted = cipher.update(hex2bin, "base64", "utf8");
    //   decrypted += cipher.final("utf8");

    //   reply.code(200);
    //   return { message: "success", hex: hex2bin, result: decrypted };
    // });

    server.get("/data/mainproduct", async (request: any, reply: any) => {
      const data: any = await this.dbMethod.getByIndex("config", 0);
      const result = {
        en: data.Item.en,
        ko: data.Item.ko,
        url: data.Item.url,
        image1: data.Item.image1,
        image2: data.Item.image2,
        image3: data.Item.image3,
      };

      reply.code(200);
      return {message: "success", result: result};
    });

    server.get("/data/commision", async (request: any, reply: any) => {
      const data: any = await this.dbMethod.getByIndex("config", 1);
      reply.code(200);
      return {
        message: "success",
        result: {commision: data.Item.commision, sale: data.Item.sale},
      };
    });
  }

  // private async cheat(phone: string) {
  //   const { data } = await Axios.get(
  //     `http://13.209.92.132:8081/search/cheat?phone=${phone}&key=${"pair2apikey"}`
  //   );
  //   const content = data.data.content;
  //   const decode = Buffer.from(content, "base64").toString();
  //   const split = decode.split(",").join("");
  //   const hex2bin = Buffer.from(split, "hex").toString();
  //   const secret: string = "7BZi5T90#ul6a!UzsZ9eViLF*9s9M1Th";
  //   const cipher = Crypto.createDecipheriv(
  //     "aes-256-cbc",
  //     secret,
  //     secret.slice(0, 16)
  //   );
  //   let decrypted = cipher.update(hex2bin, "base64", "utf8");
  //   decrypted += cipher.final("utf8");
  //   const index = decrypted.indexOf("caution");
  //   const value = decrypted.substr(index + 10, 1);

  //   return value;
  // }

  private async paymentAsk(askIndex: number, buyer: number, price: number) {
    try {
      const name = "거래";
      const paymentToken = await Payment.getToken();
      const result = await Payment.gateway(paymentToken, {
        customer_uid: buyer,
        merchant_uid: askIndex, // askindex // Date.now(),
        amount: price,
        name: name,
      });

      if (result.status === "paid") {
        return {message: "success", result: "success"};
      } else {
        return {message: "faild", result: "pay faild"};
      }
    } catch (error) {
      return {message: "faild", result: "pay faild"};
    }
  }

  private listen(): void {
    server.listen(this.port, "0.0.0.0", (err: any, address: any) => {
      if (err) throw err;
      console.log(`Server listening on ${address}`);
      server.log.info(`Server listening on ${address}`);
    });
  }

  public update(): void {
    this.userNotification();
    this.initData();
    this.get();
    this.price(); // price method collection
    this.scanLocal();
    this.admin();
    this.auth();
    this.mypage();
    this.like();
    this.payment();
    this.trade();
    this.trading();
    this.ask();
    this.shipping();
    this.etc();
    this.listen();
  }
}

export default ApiGateway;
