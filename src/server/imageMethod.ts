import * as AWS from "aws-sdk";
import { multer } from "../generic/interface";
import { s3Config } from "../config/aws";
import sharp from "sharp";
import uuid from "uuid/v4";

const s3 = new AWS.S3(s3Config);

class ImageMethod {
  public memoryStorage: any;

  constructor() {
    this.memoryStorage = multer({
      storage: multer.memoryStorage(),
      limits: { fileSize: 350 * 1024 * 1024 },
    }).array("photos", 12); // form-data key set photos
  }

  public async upload(files: any, size: string) {
    const buffer = await sharp(files.buffer) // buffer memory storage
      .resize(parseInt(size))
      .toBuffer();
    // const buffer = files.buffer;
    const mimetype: string = files.mimetype.split("/")[1];

    const params = {
      Bucket: "pair2-image-cdn",
      Key: `images/sneakers/${size}/${uuid()}.${mimetype}`,
      ACL: "public-read",
      Body: buffer
    };

    const result = await s3.upload(params).promise();
    return result;
  }

  public async uploadRelease(files: any, size: string) {
    const buffer = await sharp(files.buffer) // buffer memory storage
      .resize(parseInt(size))
      .toBuffer();
    const mimetype: string = files.mimetype.split("/")[1];

    const params = {
      Bucket: "pair2-image-cdn",
      Key: `images/release/${size}/${uuid()}.${mimetype}`,
      ACL: "public-read",
      Body: buffer
    };

    const result = await s3.upload(params).promise();
    return result;
  }

  public async uploadTrade(files: any) {
    const size: string = "800";

    const buffer = await sharp(files.buffer) // buffer memory storage
      .rotate()
      .resize(parseInt(size))
      .toBuffer();

    const mimetype: string = files.mimetype.split("/")[1];
    const params = {
      Bucket: "pair2-image-cdn",
      Key: `images/trade/${size}/${uuid()}.${mimetype}`,
      ACL: "public-read",
      Body: buffer
    };
    // console.log(params);

    const result = await s3.upload(params).promise();
    // console.log(result);
    return result;
  }

}

export default ImageMethod;