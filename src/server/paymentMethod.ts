import axios from "axios";
import iamport from "../config/payment";
import uuid from "uuid/v4";


class Payment {
  public async getToken() {
    const token = await axios({
      url: "https://api.iamport.kr/users/getToken",
      method: "post",
      headers: { "Content-Type": "application/json" },
      data: {
        imp_key: iamport.key,
        imp_secret: iamport.secret
      }
    });

    return token.data.response.access_token;
  }

  public async getBillingKey(token: string, customer_uid: string, body: any) {
    const issueBilling: any = await axios({
      url: `https://api.iamport.kr/subscribe/customers/${customer_uid}`,
      method: "post",
      headers: { "Authorization": token },
      data: {
        card_number: body.card_number,
        expiry: body.expiry,
        birth: body.birth,
        pwd_2digit: body.pwd_2digit,
      }
    });

    const { code, message } = issueBilling.data;
    if (code === 0) {
      return issueBilling.data.response;
    } else { // 빌링키 발급 실패
      return "faild";
    }
  }

  public async gateway(token: string, body: any) {
    try {
      const payment: any = await axios({
        url: `https://api.iamport.kr/subscribe/payments/again`,
        method: "post",
        headers: { "Authorization": token },
        data: {
          customer_uid: body.customer_uid,
          merchant_uid: body.merchant_uid, // payment complete id
          amount: body.amount, // price integer
          name: body.name, // 주문이름,
          // buyer_name: body.buyer_name, // "주문자이름",
          // buyer_tel: body.buyer_tel, // "주문자 전화번호",
          // buyer_email: body.buyer_email, // "주문자 이메일",
          // card_quota: body.card_quota, // 카드할부개월수. 2 이상의 integer 할부개월수 적용(결제금액 50,000원 이상 한정)
        }
      });

      const { code } = payment.data; // connect with card company

      if (code === 0) {
        return payment.data.response;
      } else {
        return "faild";
      }

    } catch (error) {
      return "faild";
    }
  }
}


export default new Payment;